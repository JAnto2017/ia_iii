package AI_3;

/**
 * Esta clase contiene 3 propiedades:
 * 1.- El nodo de partida del arco.
 * 2.- El nodo de llegada.
 * 3.- La longitud o coste del arco.
 * 
 * El constructor inicializa más rápido.
 */

public class Arco {
    protected Nodo origen;
    protected Nodo destino;
    protected double coste;

    public Arco(Nodo _origen,Nodo _destino,double _coste) {
        origen = _origen;
        destino = _destino;
        coste = _coste;
    }
}
