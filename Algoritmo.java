package AI_3;

/**
 * Clase que heredará cada uno de los cinco algoritmos a implementar.
 * 
 * Contiene dos atributos para el grafo.
 * La IHM para la salida.
 * Constructor.
 */
public abstract class Algoritmo {
    
    protected Grafo grafo;
    protected IHM ihm;

    public Algoritmo(Grafo _grafo,IHM _ihm) {
        grafo = _grafo;
        ihm = _ihm;
    }

    /**
     * método principal.
     * 
     */
    public final void Resolver() {
       grafo.Borrar();
       Run();
       ihm.MostrarResultado(grafo.ReconstruirRuta(), grafo.NodoSalida().distanciaDelInicio); 
    }

    /**
     * Es abstracto y es el único que debemos redefinir.
     */
    protected abstract void Run();
    
}
