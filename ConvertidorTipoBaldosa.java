package AI_3;

/**
 * Transforma un carácter en un tipo. Esto permite introducir un nuevo mapa de manera sencilla.
 * Los caminos se repesentan por "."
 * Árboles por "*"
 * Hierba por " " (espacio vacío)
 * Agua por "X"
 * Puentes por "="
 */
public class ConvertidorTipoBaldosa {
    public static TipoBaldosa CharToType(char c) {
        switch (c) {
            case ' ':
                return TipoBaldosa.Hierba;                
            case '*':
                return TipoBaldosa.Arbol;
            case '=':
                return TipoBaldosa.Puente;
            case 'X':
                return TipoBaldosa.Agua;
            case '.':
                return TipoBaldosa.Camino;
            
            default:
                break;
        }
        return null;
    }
}
