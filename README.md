# Búsqueda de rutas
Código genérico, lo que permitirá agregar fácilmente nuevos métodos de resolución.
La primera etapa define el grafo. Nodos. Arcos que los enlaza. Grafo completo.

### Implementación de los nodos
Los nodos son las estructuras básicas de nuestros grafos.
Los nodos necesitan:
* El precursor, que es un nodo.
* Distancia desde el inicio.
* Distancia estimada hasta el destino.

### Clase que representa los arcos
Definición de los arcos, precisan de tres propiedades:
* Nodo de partida del arco.
* Nodo de llegada.
* Longitud o coste del arco.

### Grafos

Hasta aquí es el programa genérico. A partir de este punto se implementan:

### Interfaz IHM
El programa se utiliza, en este caso, desde una consola.

### Algoritmo genérico

## Implementación de los diferentes algoritmos
### Búsqueda en profundidad
### Búsqueda en anchura
### Algoritmo de Bellman-Ford
### Algoritmo de Dijkstra
### Algoritmo A*

## Aplicación al mapa
Las convenciones utilizadas en las representaciones del mapa son:
* Caminos => "."
* Árbol   => "*"
* Hierba  => " " (espacio en blanco)
* Agua    => "X"
* Puentes => "="

## Comprobación de rendimiento --------------------------------------

> El orden de visita de los nodos para el recorrido en profundidad no se ha definido tras una reflexión, sino al azar desde el código. El algoritmo resulta rápido en el primer mapa y mucho menos en el segundo.

> La búsqueda en anchura encuentra caminos globalmente más cortos que la de profundidad, puesto que todos los archos tienen prácticamente el mismo coste.

> De los tres algoritmos de búsqueda de rutas óptimas, vemos que Bellman-Ford es el más eficaz sobre el mapa grande. Este algoritmo no ordena los nodos para encontrar el más próximo, a diferencia de Dijkstra y A*, lo que permite ganar tiempo en mapas grandes.

> Como conslusión:
> 1. La búsqueda en anchura es mejor que en profundidad.
> 2. Bellman-Ford es el mejor adaptado a problemas sencillos.
> 3. Dijkstra y A* son equivalentes en su conjunto. Dijkstra mejor adaptado a laberintos. Para zonas libres de obstáculos, es preferible A*.

Solución Algoritmo: Profundidad

![](/ia_iii_01.png "Solución primer algoritmo")

Solución Algoritmo: Anchura

![](/ia_iii_02.png "Solución primer algoritmo")

Solución Algoritmo: Bellman-Ford

![](/ia_iii_03.png "Solución primer algoritmo")

Solución Algoritmo: Dijkstra

![](/ia_iii_04.png "Solución primer algoritmo")

Solución Algoritmo: A*

![](/ia_iii_05.png "Solución primer algoritmo")

Conjunto de soluciones:

![](/ia_iii_06.png "Solución primer algoritmo")

Mapa 1:

![](/mapa_01.png "Mapa 1")

Mapa 2:

![](/mapa_01.png "Mapa 2")


## Push a new repository --------------------------------------------
* git clone https://gitlab.com/JAnto20117/...
* cd <nombre1>
* git switch -c main
* touch README.md
* git add README.md
* git commit -m "add README"
* git push -u origin main

## Push an existing folder ------------------------------------------
* cd existing_folder
* git init --initial-branch=main
* git remote add origin https://gitlab.com/JAnto2017/ia_iii.git
* git add .
* git commit -m "Initial commit"
* git push -u origin main

## Push an existing Git repository ----------------------------------
* cd existing_repo
* git remote rename origin old-origin
* git remote add origin https://gitlab.com/JAnto2017/ia_iii.git
* git push -u origin --all
* git push -u origin --tags
