package AI_3;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;

public class Aplicacion implements IHM {
    //programa main
    public static void main(String[] args) {
        System.out.println("Búsqueda de rutas");
        Aplicacion app = new Aplicacion();
        app.Ejecutar();
    }

    //método que proviene de la interfaz, para visualizar el resultado
    @Override
    public void MostrarResultado(String ruta, double distancia) {
        System.out.println("Ruta (tamaño: " + distancia + "): " + ruta);
    }

    /**
     * 
     * @param nombre
     * @param grafo
     */
    private void EjecutarAlgoritmo(String nombre, Grafo grafo) {
        //inicializacion
        LocalDateTime inicio;
        LocalDateTime fin;
        Duration duracion;
        Algoritmo algo = null;

        //creacion del algoritmo
        switch (nombre) {
            case "Profundidad":
                algo = new BusquedaEnProfundidad(grafo, this);
                break;
            case "Anchura":
                algo = new BusquedaEnAnchura(grafo, this);
                break;
            case "Bellman-Ford":
                algo = new BellmanFord(grafo, this);
                break;
            case "Dijkstra":
                algo = new Dijkstra(grafo, this);
                break;
            case "A":
                algo = new AStar(grafo, this);
                break;            
        }

        //resolucion
        System.out.println("Algoritmo: " + nombre);
        inicio = LocalDateTime.now();
        algo.Resolver();
        fin = LocalDateTime.now();
        duracion = Duration.between(inicio, fin);

        System.out.println("Duración (ms): " + duracion.toMillis() + "\n");
    }

    /**
     * Permite ejecutar los cinco algoritmos.
     * @param grafo
     */
    private void EjecutarAlgoritmos(Grafo grafo) {
        EjecutarAlgoritmo("Profundidad", grafo);
        EjecutarAlgoritmo("Anchura", grafo);
        EjecutarAlgoritmo("Bellman-Ford", grafo);
        EjecutarAlgoritmo("Dijkstra", grafo);
        EjecutarAlgoritmo("A", grafo);
    }

    /**
     * Creamos mapa.
     * Ejecutamos distintos algoritmos.
     */
    private void Ejecutar() {
        //caso primer mapa
        String mapaStr =    "..  XX   .\n" + 
                            "*.  *X  *.\n" +
                            " .  XX ...\n" +
                            " .* X *.* \n" +
                            " ...=...  \n" +
                            " .* X     \n" +
                            " .  XXX*  \n" +
                            " .  * =   \n" +
                            " .... XX  \n" +
                            "   *.  X* ";
        
        Mapa mapa1 = new Mapa(mapaStr, 0, 0, 9, 9);
        EjecutarAlgoritmos(mapa1);                        

        //caso segundo mapa
        mapaStr =   "...*    X .*    *    \n" +
                    " *..*   *X  .........\n" +
                    "   .     =    *.*  *.\n" +
                    "  *.   * XXXX  .    .\n" +
                    "XXX=XX   X *XX=XXX* .\n" +
                    "  *.*X   =  X*.  X   \n" +
                    "   . X * X  X . *X*  \n" +
                    "*  .*XX=XX *X . XXXX \n" +
                    " ....  .... X . X    \n" +
                    " . *....* . X*. =  * ";
        
        Mapa mapa2 = new Mapa(mapaStr, 0, 0, 9, 19);
        EjecutarAlgoritmos(mapa2);
    }
}
