package AI_3;

import java.util.ArrayList;
import java.util.Arrays;

public class Mapa implements Grafo {
    Baldosa[][] baldosas;
    int numFilas;
    int numColumnas;
    Baldosa nodoPartida;
    Baldosa nodoLlegada;

    ArrayList<Nodo> listaNodos = null;
    ArrayList<Arco> listaArcos = null;

    public Mapa(String _mapa,int _filaSalida,int _columnaSalida,int _filaLlegada,int _columnaLlegada) {
        //creación del array de baldosas
        String[] filas = _mapa.split("\n");
        numFilas = filas.length;
        numColumnas = filas[0].length();
        baldosas = new Baldosa[numFilas][];

        //relleno
        for (int i = 0; i < numFilas; i++) {
            baldosas[i] = new Baldosa[numColumnas];
            for (int j = 0; j < numColumnas; j++) {
                TipoBaldosa tipo = ConvertidorTipoBaldosa.CharToType(filas[i].charAt(j));
                baldosas[i][j] = new Baldosa(tipo, i, j);
            }
        }

        //partida y llegada
        nodoPartida = baldosas[_filaSalida][_columnaSalida];
        nodoPartida.distanciaDelInicio = nodoPartida.Coste();

        nodoLlegada = baldosas[_filaLlegada][_columnaLlegada];

        //lista de nodos y de arcos
        ListaNodos();
        ListaArcos();
    }

    @Override
    public Nodo NodoPartida() {
        return nodoPartida;
    }

    @Override
    public Nodo NodoSalida() {
        return nodoLlegada;
    }

    /**
     * Devuelve la lista de todos los nodos.
     * Si no se ha creado la lista en una llamada anterio, se crea ahora.
     * Para ello, recorremos todas las filas de la tabla y agregamos todas las columnas de una vez a la lista.
     */
    @Override
    public ArrayList<Nodo> ListaNodos() {
        if (listaNodos == null) {
            listaNodos = new ArrayList<>();
            for (int i = 0; i < numFilas; i++) {
                listaNodos.addAll(Arrays.asList(baldosas[i]));
            }
        }
        return listaNodos;
    }

    /**
     * Devuelve los nodos adyacentes al nodo que se pasa como parámetro.
     * Comprobamos los cuatro vecinos y si son alcanzables los agregaremos a nuestra lista, que luego devolveremos.
     */
    @Override
    public ArrayList<Nodo> ListaNodosAdyacentes(Nodo origen) {
        //Inicialización
        ArrayList<Nodo> listaNodosSalientes = new ArrayList<>();
        int fila = ((Baldosa)origen).fila;
        int columna = ((Baldosa)origen).columna;

        //vecino de la derecha
        if (columna - 1 >= 0 && baldosas[fila][columna-1].Accesible()) {
            listaNodosSalientes.add(baldosas[fila][columna-1]);
        }

        //vecino de la izquierda
        if (columna + 1 < numColumnas && baldosas[fila][columna+1].Accesible()) {
            listaNodosSalientes.add(baldosas[fila][columna+1]);
        }

        //vecino de arriba
        if (fila - 1 >= 0 && baldosas[fila-1][columna].Accesible()) {
            listaNodosSalientes.add(baldosas[fila-1][columna]);
        }

        //vecino de abaja
        if (fila + 1 < numFilas && baldosas[fila+1][columna].Accesible()) {
            listaNodosSalientes.add(baldosas[fila+1][columna]);
        }
        return listaNodosSalientes;
    }

    /**
     * Devuelven el número de nodos.
     * Devuelve el nº de casillas del tablero.
     */
    public int NumeroNodos() {
        return numFilas * numColumnas;
    }

    @Override
    public ArrayList<Arco> ListaArcosSalientes(Nodo origen) {
        ArrayList<Arco> listaArcosSalientes = new ArrayList<>();
        int fila = ((Baldosa)origen).fila;
        int columna = ((Baldosa)origen).columna;

        if (baldosas[fila][columna].Accesible()) {
            //derecha
            if (columna - 1 >= 0 && baldosas[fila][columna-1].Accesible()) {
                listaArcosSalientes.add(new Arco(baldosas[fila][columna], baldosas[fila][columna-1], baldosas[fila][columna-1].Coste()));
            }

            //izquierda
            if (columna + 1 < numColumnas && baldosas[fila][columna+1].Accesible()) {
                listaArcosSalientes.add(new Arco(baldosas[fila][columna], baldosas[fila][columna+1], baldosas[fila][columna+1].Coste()));
            }

            //arriba
            if (fila - 1 >= 0 && baldosas[fila-1][columna].Accesible()) {
                listaArcosSalientes.add(new Arco(baldosas[fila][columna], baldosas[fila-1][columna], baldosas[fila-1][columna].Coste()));
            }

            //abajo
            if (fila + 1 < numFilas && baldosas[fila+1][columna].Accesible()) {
                listaArcosSalientes.add(new Arco(baldosas[fila][columna], baldosas[fila+1][columna], baldosas[fila+1][columna].Coste()));
            }
        }
        return listaArcosSalientes;
    }

    @Override
    public ArrayList<Arco> ListaArcos() {
        if (listaArcos == null) {
            listaArcos = new ArrayList<>();

            //recorrido de los nodos
            for (int linea = 0; linea < numFilas; linea++) {
                for (int columna = 0; columna < numColumnas; columna++) {
                    ArrayList<Arco> arcos = ListaArcosSalientes(baldosas[linea][columna]);
                    listaArcos.addAll(arcos);
                }
            }
        }
        return listaArcos;
    }

    @Override
    public double Coste(Nodo salida, Nodo llegada) {
        return ((Baldosa)llegada).Coste();
    }

    /**
     * crea cadena que contenga distintos nodos para ir desde origen hasta destino.
     * Es necesario recorrer predecesores.
     */
    @Override
    public String ReconstruirRuta() {
        //inicialización
        String ruta = "";
        Baldosa nodoEnCurso = nodoLlegada;
        Baldosa nodoAnterior = (Baldosa) nodoLlegada.precursor;

        //bucle sobre los nodos de la ruta
        while (nodoAnterior != null) {
            ruta = "-" +  nodoEnCurso.toString() + ruta;
            nodoEnCurso = nodoAnterior;
            nodoAnterior = (Baldosa) nodoEnCurso.precursor;
        }

        ruta = nodoEnCurso.toString() + ruta;
        return ruta;
    }

    /**
     * Para el algoritmo A*, es preciso conocer la distancia estimada hasta destino.
     * Utiliza distancia Manhattan.
     * Se trata del número de casillas horizontales + nº casillas verticales para ir desde la casilla en curso hasta la casilla destino.
     */
    @Override
    public void CalcularDistanciasEstimadas() {
        for (int fila = 0; fila < numFilas; fila++) {
            for (int columna = 0; columna < numColumnas; columna++) {
                baldosas[fila][columna].distanciaEstimada = Math.abs(nodoLlegada.fila - fila) + Math.abs(nodoLlegada.columna - columna);
            }
        }
    }

    @Override
    public void Borrar() {
        //borrar las listas
        listaNodos = null;
        listaArcos = null;

        //borrar distancias y precursores
        for (int fila = 0; fila < numFilas; fila++) {
            for (int columna = 0; columna < numColumnas; columna++) {
                baldosas[fila][columna].distanciaDelInicio = Double.POSITIVE_INFINITY;
                baldosas[fila][columna].precursor = null;
            }
        }

        //nodo inicial
        nodoPartida.distanciaDelInicio = nodoPartida.Coste();
    }
}
