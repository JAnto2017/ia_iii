package AI_3;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Búsqueda de rutas en profundidad.
 * Conserva lista de todos los nodos que todavía no se han visitado.
 * Se parte del nodo inicial y se agrega a la pila todos los nodos vecinos actualizándolos.
 * Si el nodo que desapilamos es el nodo de destino, hemos terminado.
 * 
 * Usamos la clase Stack para gestionar la pila. Tiene dos métdos: push(), pop()
 */
public class BusquedaEnProfundidad extends Algoritmo {
    
    //constructor
    public BusquedaEnProfundidad(Grafo _grafo, IHM _ihm) {
        super(_grafo, _ihm);
    }

    //método de resolución
    @Override
    protected void Run() {
        //creación de la lista de nodos no visitados y de la pila
        ArrayList<Nodo> nodosNoVisitados = grafo.ListaNodos();
        Stack<Nodo> nodosAVisitar = new Stack<>();
        nodosAVisitar.push(grafo.NodoPartida());
        nodosNoVisitados.remove(grafo.NodoPartida());

        //inicialización de la salida
        Nodo nodoSalida = grafo.NodoSalida();
        boolean salidaEncontrada = false;

        //bucle principal
        while (!salidaEncontrada && nodosAVisitar.size() != 0) {
            Nodo nodoEnCurso = nodosAVisitar.pop();
            if (nodoEnCurso.equals(nodoSalida)) {
                salidaEncontrada = true;
            } else {
                for (Nodo n : grafo.ListaNodosAdyacentes(nodoEnCurso)) {
                    if (nodosNoVisitados.contains(n)) {
                        nodosNoVisitados.remove(n);
                        n.precursor = nodoEnCurso;
                        n.distanciaDelInicio = nodoEnCurso.distanciaDelInicio + grafo.Coste(nodoEnCurso, n);
                        nodosAVisitar.push(n);
                    }
                }
            }
        }
    }
}
