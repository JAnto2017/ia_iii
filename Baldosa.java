package AI_3;

public class Baldosa extends Nodo {
    protected TipoBaldosa tipo;
    protected int fila;
    protected int columna;

    //constructor
    public Baldosa(TipoBaldosa _tipo,int _fila,int _columna) {
        tipo = _tipo;
        fila = _fila;
        columna = _columna;
    }

    /**
     * Indica si es posible ir hasta una casilla o no.
     * Observa tipo de casilla: caminos, hierba, puentes.
     * @return
     */
    public boolean Accesible() {
        return (tipo.equals(TipoBaldosa.Camino) || tipo.equals(TipoBaldosa.Hierba) || tipo.equals(TipoBaldosa.Puente));
    }

    /**
     * Indica el coste de las casillas.
     * Los caminos tienen un coste en puntos de acción igual a 1.
     * La hierba y los puentes igual a 2.
     * Devolvemos distancia infinita para las casillas que no son alcanzables: árbol, agua.
     * @return
     */
    public double Coste() {
        switch (tipo) {
            case Camino:
                return 1;
            case Puente:
            case Hierba:
                return 2;
        
            default:
                return Double.POSITIVE_INFINITY;
        }
    }

    /**
     * Sobrecarga del método.
     * 
     */
    @Override
    public String toString() {
        return "[" + fila + ";" + columna + ";" + tipo.toString() + "]";
    }
}
