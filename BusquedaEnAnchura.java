package AI_3;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Permite reemplazar la pila Stack por una fila
 * representada por una lista encadenada: LinkedList y los métodos push/pop
 * por add para agregar al final y removeFirst, para eliminar al principio.
 * 
 */
public class BusquedaEnAnchura extends Algoritmo{
    
    //constructor
    public BusquedaEnAnchura(Grafo _grafo,IHM _ihm) {
        super(_grafo, _ihm);
    }

    //método de resolución
    @Override
    protected void Run() {
        //creación de la lista de nodos no visitados y de la pila
        ArrayList<Nodo> nodosNoVisitados = grafo.ListaNodos();
        LinkedList<Nodo> nodosAVisitar = new LinkedList<>();
        nodosAVisitar.add(grafo.NodoPartida());
        nodosNoVisitados.remove(grafo.NodoPartida());

        //inicialización de la salida
        Nodo nodoSalida = grafo.NodoSalida();
        boolean salidaEncontrada = false;

        //bucle principal
        while (!salidaEncontrada && nodosAVisitar.size() != 0) {
            Nodo nodoEnCurso = nodosAVisitar.removeFirst();

            if (nodoEnCurso.equals(nodoSalida)) {
                //se ha terminado el algoritmo
                salidaEncontrada = true;
            } else {
                //se agregan los vecinos todavía no visitados
                for (Nodo n : grafo.ListaNodosAdyacentes(nodoEnCurso)) {
                    if (nodosNoVisitados.contains(n)) {
                        nodosNoVisitados.remove(n);
                        n.precursor = nodoEnCurso;
                        n.distanciaDelInicio = nodoEnCurso.distanciaDelInicio + grafo.Coste(nodoEnCurso, n);
                        nodosAVisitar.add(n);
                    }
                }
            }
        }
    }
}
