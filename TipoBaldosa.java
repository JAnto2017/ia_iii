package AI_3;

/**
 * Cada casilla del mapa es una baldosa de distintos tipos.
 */
public enum TipoBaldosa {
    Hierba,
    Arbol,
    Agua,
    Puente,
    Camino
};
