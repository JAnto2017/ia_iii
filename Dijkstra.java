package AI_3;

import java.util.ArrayList;

/**
 * Se aplica sobre Arcos y sobre Nodo.
 * En cada iteración se selecciona un Nodo y se aplican todos los Arcos salientes, actualizando los nodos adyacentes para los que hemos encontrado un camino más corto que el previamente anotado.
 * 
 * Para seleccionar el nodo utilizado, tomamos aquel cuya distancia al origen sea la menor, una vez recorrida nuestra lista.
 * 
 */
public class Dijkstra extends Algoritmo {
    
    //constructor
    public Dijkstra(Grafo _grafo, IHM _ihm) {
        super(_grafo, _ihm);
    }

    //método principal
    @Override
    protected void Run() {
        //inicialización
        ArrayList<Nodo> listaNodos = grafo.ListaNodos();
        boolean salidaEncontrada = false;

        //bucle principal
        while (listaNodos.size() != 0 && !salidaEncontrada) {
            //búsqueda del nodo con la distancia menor
            Nodo nodoEnCurso = listaNodos.get(0);

            for (Nodo nodo : listaNodos) {
                if (nodo.distanciaDelInicio < nodoEnCurso.distanciaDelInicio) {
                    nodoEnCurso = nodo;
                }
            }

            if (nodoEnCurso.equals(grafo.NodoSalida())) {
                salidaEncontrada = true;
            } else {
                ArrayList<Arco> arcosSalientes = grafo.ListaArcosSalientes(nodoEnCurso);

                for (Arco arco : arcosSalientes) {
                    if (arco.origen.distanciaDelInicio + arco.coste < arco.destino.distanciaDelInicio) {
                        arco.destino.distanciaDelInicio = arco.origen.distanciaDelInicio + arco.coste;
                        arco.destino.precursor = arco.origen;
                    }
                }

                listaNodos.remove(nodoEnCurso);
            }
        }
    }
}
