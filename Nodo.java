package AI_3;

/**
 * Contendrá la información necesaria para los algoritmos.
 * Los nodos necesitan 3 datos:
 * 1.- El precursor, que es un nodo.
 * 2.- La distancia desde el inicio.
 * 3.- La distancia estimada hasta el destino (si es necesario)
 * 
 */

public abstract class Nodo {
    public Nodo precursor = null;
    public double distanciaDelInicio = Double.POSITIVE_INFINITY;
    public double distanciaEstimada;
}
