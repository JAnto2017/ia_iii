package AI_3;

import java.util.ArrayList;

public class AStar extends Algoritmo {
    
    //constructor
    public AStar(Grafo _grafo,IHM _ihm) {
        super(_grafo, _ihm);
    }

    //método principal
    @Override
    protected void Run() {
        //inicialización
        grafo.CalcularDistanciasEstimadas();
        ArrayList<Nodo> listaNodos = grafo.ListaNodos();
        boolean salidaEncontrada = false;

        //bucle principal
        while (listaNodos.size() != 0 && !salidaEncontrada) {
            //búsqueda del nodo con la salida menor
            Nodo nodoEnCurso = listaNodos.get(0);

            for (Nodo nodo : listaNodos) {
                if (nodo.distanciaDelInicio + nodo.distanciaEstimada < nodoEnCurso.distanciaDelInicio + nodoEnCurso.distanciaEstimada) {
                    nodoEnCurso = nodo;
                }
            }

            if (nodoEnCurso.equals(grafo.NodoSalida())) {
                //se ha encontrado la salida
                salidaEncontrada = true;
            } else {
                //se aplican los arcos salientes de este nodo
                ArrayList<Arco> arcosSalientes = grafo.ListaArcosSalientes(nodoEnCurso);

                for (Arco arco : arcosSalientes) {
                    if (arco.origen.distanciaDelInicio + arco.coste < arco.destino.distanciaDelInicio) {
                        arco.destino.distanciaDelInicio = arco.origen.distanciaDelInicio + arco.coste;
                        arco.destino.precursor = arco.origen;
                    }
                }

                listaNodos.remove(nodoEnCurso);
            }
        }
    }
}
