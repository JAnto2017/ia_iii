package AI_3;

/**
 * Interface IHM
 * Programa se utiliza desde consola.
 * Mostrará el resultado como un camino en forma de cadena e indicará la distancia obtenida.
 */
public interface IHM {
    void MostrarResultado(String ruta,double distancia);
}