package AI_3;

import java.util.ArrayList;

/**
 * Contendrá todos los métodos que definirá cada Grafo
 */
public interface Grafo {
    
    Nodo NodoPartida();
    Nodo NodoSalida();

    /**
     * 2 métodos permiten recuperar todos los nodos en lista
     * 2 métodos para recuperar todos los arcos.
     */

    ArrayList<Nodo> ListaNodos();
    ArrayList<Nodo> ListaNodosAdyacentes(Nodo origen);

    ArrayList<Arco> ListaArcos();
    ArrayList<Arco> ListaArcosSalientes(Nodo origen);

    /**
     * Funciones complementarias:
     * > contar número de nodos.
     * > devolver distancia entre dos nodos.
     * > calcular distancia hasta destino.
     * > reconstruir el camino a partir de los nodos precedentes.
     * > restablecer grafo a su estado inicial.
     */

    int NumeroNodos();
    double Coste(Nodo salida, Nodo llegada);
    String ReconstruirRuta();
    void CalcularDistanciasEstimadas();
    void Borrar();

}

