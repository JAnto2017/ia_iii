package AI_3;

import java.util.ArrayList;

/**
 * Garantiza el camino más corto.
 * Aplica todos los arcos.
 * Actualiza los nodos si se encuentra algún camino más corto, tantas veces como número de nodos - 1.
 * Se detiene si no es posible mejorar las distancias encontradas.
 */
public class BellmanFord extends Algoritmo{
    //constructor
    public BellmanFord(Grafo _grafo,IHM _ihm) {
        super(_grafo, _ihm);
    }

    //método de resolución
    @Override
    protected void Run() {
        //inicialización
        boolean distanciaCambiada = true;
        int i = 0;
        ArrayList<Arco> listaArcos = grafo.ListaArcos();

        //bucle principal
        int numBucleMax = grafo.NumeroNodos() - 1;

        while (i < numBucleMax && distanciaCambiada) {
            distanciaCambiada = false;

            for (Arco arco : listaArcos) {
                if (arco.origen.distanciaDelInicio + arco.coste < arco.destino.distanciaDelInicio) {
                    //se ha encontrado una ruta más corta
                    arco.destino.distanciaDelInicio = arco.origen.distanciaDelInicio + arco.coste;
                    arco.destino.precursor = arco.origen;
                    distanciaCambiada = true;
                }
            }
            i++;
        }

        //comprueba si es negativa
        for (Arco arco : listaArcos) {
            if (arco.origen.distanciaDelInicio + arco.coste < arco.destino.distanciaDelInicio) {
                System.err.println("Bucle negativo - no hay ninguna ruta más corta");
            }
        }
    }
}
